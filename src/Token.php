<?php
namespace Plox;

class Token
{
    /**
     * One of TokenType
     *
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $lexeme;

    /**
     * @var string|null
     */
    private $literal;

    /**
     * Position in lox code of the start of the lexeme
     *
     * @var int
     */
    private $position;

    /**
     * @param string $type
     * @param string $lexeme
     * @param string|null $literal
     * @param int $position
     */
    public function __construct(string $type, string $lexeme, ?string $literal, int $position)
    {
        $this->type = $type;
        $this->lexeme = $lexeme;
        $this->literal = $literal;
        $this->position = $position;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getLexeme(): string
    {
        return $this->lexeme;
    }

    /**
     * @return string|null
     */
    public function getLiteral(): ?string
    {
        return $this->literal;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return sprintf(
            'Token{type: %s, lexeme: %s, literal: %s}',
            $this->getType(),
            $this->getLexeme(),
            $this->getLiteral() ?? '<null>'
        );
    }
}
<?php
namespace Plox;

final class TokenType
{
    // single character tokens
    const T_LEFT_PAREN = 'LEFT_PAREN';
    const T_RIGHT_PAREN = 'RIGHT_PAREN';
    const T_LEFT_BRACE = 'LEFT_BRACE';
    const T_RIGHT_BRACE = 'RIGHT_BRACE';
    const T_COMMA = 'COMMA';
    const T_DOT = 'DOT';
    const T_MINUS = 'MINUS';
    const T_PLUS = 'PLUS';
    const T_SEMICOLON = 'SEMICOLON';
    const T_SLASH = 'SLASH';
    const T_STAR = 'STAR';

    // single or double character tokens
    const T_BANG = 'BANG';
    const T_BANG_EQUAL = 'BANG_EQUAL';
    const T_EQUAL = 'EQUAL';
    const T_EQUAL_EQUAL = 'EQUAL_EQUAL';
    const T_GREATER = 'GREATER';
    const T_GREATER_EQUAL = 'GREATER_EQUAL';
    const T_LESS = 'LESS';
    const T_LESS_EQUAL = 'LESS_EQUAL';

    // literals
    const T_IDENTIFIER = 'IDENTIFIER';
    const T_STRING = 'STRING';
    const T_NUMBER = 'NUMBER';

    // keywords
    const T_AND = 'AND';
    const T_CLASS = 'CLASS';
    const T_ELSE = 'ELSE';
    const T_FALSE = 'FALSE';
    const T_FUN = 'FUN';
    const T_FOR = 'FOR';
    const T_IF = 'IF';
    const T_NIL = 'NIL';
    const T_OR = 'OR';
    const T_PRINT = 'PRINT';
    const T_RETURN = 'RETURN';
    const T_SUPER = 'SUPER';
    const T_THIS = 'THIS';
    const T_TRUE = 'TRUE';
    const T_VAR = 'VAR';
    const T_WHILE = 'WHILE';

    const T_EOF = 'EOF';

    /**
     * Prevent instantiating this class
     */
    private function __construct() {}
}
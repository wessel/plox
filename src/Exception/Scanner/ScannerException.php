<?php
namespace Plox\Exception\Scanner;

abstract class ScannerException extends \RuntimeException
{
    /**
     * @var int
     */
    private $position;

    /**
     * @param int $position
     */
    public function __construct(int $position)
    {
        parent::__construct();

        $this->position = $position;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }
}
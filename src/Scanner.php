<?php
namespace Plox;

use Plox\Exception\Scanner\UnexpectedCharacter;
use Plox\Exception\Scanner\UnterminatedString;
use Plox\Reader\StringReader;

class Scanner
{
    private const KEYWORDS = [
        'and' => TokenType::T_AND,
        'class' => TokenType::T_CLASS,
        'else' => TokenType::T_ELSE,
        'false' => TokenType::T_FALSE,
        'fun' => TokenType::T_FUN,
        'for' => TokenType::T_FOR,
        'if' => TokenType::T_IF,
        'nil' => TokenType::T_NIL,
        'or' => TokenType::T_OR,
        'print' => TokenType::T_PRINT,
        'return' => TokenType::T_RETURN,
        'super' => TokenType::T_SUPER,
        'this' => TokenType::T_THIS,
        'true' => TokenType::T_TRUE,
        'var' => TokenType::T_VAR,
        'while' => TokenType::T_WHILE,
    ];

    /**
     * @var StringReader
     */
    private $reader;

    /**
     * @var Token[]
     */
    private $tokens;

    /**
     * @param StringReader $reader
     */
    public function __construct(StringReader $reader)
    {
        $this->reader = $reader;
        $this->tokens = [];
    }

    /**
     * @return Token[]
     */
    public function scanTokens(): array
    {
        $this->tokens = [];

        while (!$this->reader->isAtEnd()) {
            $this->scanToken();
        }

        $this->tokens[] = new Token(TokenType::T_EOF, '', null, $this->reader->getCursor());

        return $this->tokens;
    }

    private function scanToken(): void
    {
        $this->reader->startSelection();
        $char = $this->reader->advance();

        switch ($char) {
            // ignore whitespace
            case ' ':
            case "\n":
            case "\r":
            case "\t":
                return;

            // single character tokens
            case '(': $this->addToken(TokenType::T_LEFT_PAREN); return;
            case ')': $this->addToken(TokenType::T_RIGHT_PAREN); return;
            case '{': $this->addToken(TokenType::T_LEFT_BRACE); return;
            case '}': $this->addToken(TokenType::T_RIGHT_BRACE); return;
            case ',': $this->addToken(TokenType::T_COMMA); return;
            case '.': $this->addToken(TokenType::T_DOT); return;
            case '-': $this->addToken(TokenType::T_MINUS); return;
            case '+': $this->addToken(TokenType::T_PLUS); return;
            case ';': $this->addToken(TokenType::T_SEMICOLON); return;
            case '*': $this->addToken(TokenType::T_STAR); return;

            // single or double character tokens
            case '!': $this->addToken($this->reader->match('=') ? TokenType::T_BANG_EQUAL : TokenType::T_BANG); return;
            case '=': $this->addToken($this->reader->match('=') ? TokenType::T_EQUAL_EQUAL : TokenType::T_EQUAL); return;
            case '<': $this->addToken($this->reader->match('=') ? TokenType::T_LESS_EQUAL : TokenType::T_LESS); return;
            case '>': $this->addToken($this->reader->match('=') ? TokenType::T_GREATER_EQUAL : TokenType::T_GREATER); return;

            // slash or comment
            case '/':
                if ($this->reader->match('/')) {
                    while ($this->reader->peek() !== "\n" && !$this->reader->isAtEnd()) {
                        $this->reader->advance(); // ignore the comment until the end of the line
                    }
                } else {
                    $this->addToken(TokenType::T_SLASH);
                }
                return;

            // string
            case '"':
                $this->string();
                return;
        }

        if (ctype_digit($char)) {
            $this->number();
        } elseif (ctype_alpha($char)) {
            $this->identifierOrKeyword();
        } else {
            throw new UnexpectedCharacter($this->reader->getCursor() - 1);
        }
    }

    private function string(): void
    {
        while ($this->reader->peek() !== '"' && !$this->reader->isAtEnd()) {
            $this->reader->advance();
        }

        if ($this->reader->isAtEnd()) {
            throw new UnterminatedString($this->reader->getCursor() - 1);
        }

        $this->reader->advance(); // consume the closing "

        $lexeme = $this->reader->endSelection();
        $this->tokens[] = new Token(TokenType::T_STRING, $lexeme, substr($lexeme, 1, strlen($lexeme) - 2), $this->reader->getStart());
    }

    private function number(): void
    {
        while (ctype_digit($this->reader->peek())) {
             $this->reader->advance();
        }

        if ($this->reader->match('.')) {
            while (ctype_digit($this->reader->peek())) {
                $this->reader->advance();
            }
        }

        $lexeme = $this->reader->endSelection();
        $this->tokens[] = new Token(TokenType::T_NUMBER, $lexeme, $lexeme, $this->reader->getStart());
    }

    private function identifierOrKeyword(): void
    {
        while (ctype_alpha($this->reader->peek()) && !$this->reader->isAtEnd()) {
            $this->reader->advance();
        }

        $lexeme = $this->reader->endSelection();
        $type = self::KEYWORDS[$lexeme] ?? TokenType::T_IDENTIFIER;

        $this->tokens[] = new Token($type, $lexeme, null, $this->reader->getStart());
    }

    /**
     * @param string $type
     * @param string|null $literal
     */
    private function addToken(string $type, string $literal = null): void
    {
        $lexeme = $this->reader->endSelection();
        $this->tokens[] = new Token($type, $lexeme, $literal, $this->reader->getStart());
    }
}
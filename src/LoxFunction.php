<?php
namespace Plox;

use Plox\AST\Statement\FunctionStmt;
use Plox\Exception\Interpreter\ReturnValue;

class LoxFunction implements LoxCallable
{
    /**
     * @var FunctionStmt
     */
    private $declaration;

    /**
     * @var Environment
     */
    private $closure;

    /**
     * LoxFunction constructor.
     * @param FunctionStmt $declaration
     * @param Environment $closure
     */
    public function __construct(FunctionStmt $declaration, Environment $closure)
    {
        $this->declaration = $declaration;
        $this->closure = $closure;
    }

    /**
     * @return int
     */
    public function arity(): int
    {
        return count($this->declaration->getParams());
    }

    /**
     * @param Interpreter $interpreter
     * @param mixed[] $arguments
     * @return mixed
     */
    public function call(Interpreter $interpreter, array $arguments)
    {
        $environment = new Environment($this->closure);

        for ($i = 0; $i < count($arguments); $i++) {
            $environment->define($this->declaration->getParams()[$i]->getLexeme(), $arguments[$i]);
        }

        try {
            $interpreter->executeBlock($this->declaration->getBody(), $environment);
        } catch (ReturnValue $returnValue) {
            return $returnValue->getValue();
        }

        return null;
    }
}
<?php
namespace Plox;

use Plox\AST\Expression\Assign;
use Plox\AST\Expression\Binary;
use Plox\AST\Expression\Call;
use Plox\AST\Expression\Expression;
use Plox\AST\Expression\ExpressionVisitor;
use Plox\AST\Expression\Grouping;
use Plox\AST\Expression\Literal;
use Plox\AST\Expression\Logical;
use Plox\AST\Expression\Unary;
use Plox\AST\Expression\Variable;
use Plox\AST\Statement\BlockStmt;
use Plox\AST\Statement\ExpressionStmt;
use Plox\AST\Statement\FunctionStmt;
use Plox\AST\Statement\IfStmt;
use Plox\AST\Statement\PrintStmt;
use Plox\AST\Statement\ReturnStmt;
use Plox\AST\Statement\Statement;
use Plox\AST\Statement\StatementVisitor;
use Plox\AST\Statement\VariableStmt;
use Plox\AST\Statement\WhileStmt;
use Plox\Exception\Interpreter\ReturnValue;

class Interpreter implements ExpressionVisitor, StatementVisitor
{
    /**
     * @var Environment
     */
    private $globals;

    /**
     * @var Environment
     */
    private $environment;

    /**
     * Interpreter constructor.
     * @param Environment $globals
     */
    public function __construct(Environment $globals)
    {
        $this->globals = $globals;
    }

    /**
     * @param array $statements
     * @throws ReturnValue
     */
    public function interpret(array $statements): void
    {
        $this->executeBlock($statements, $this->globals);
    }

    /**
     * @param Statement $statement
     */
    public function execute(Statement $statement): void
    {
        $statement->accept($this);
    }

    /**
     * @param Expression $expression
     * @return mixed
     */
    public function evaluate(Expression $expression)
    {
        return $expression->accept($this);
    }

    /**
     * @param ExpressionStmt $statement
     */
    public function visitExpressionStmt(ExpressionStmt $statement): void
    {
        $this->evaluate($statement->getExpression());
    }

    /**
     * @param PrintStmt $statement
     */
    public function visitPrintStmt(PrintStmt $statement): void
    {
        $value = $this->evaluate($statement->getExpression());

        echo $this->stringify($value), PHP_EOL;
    }

    /**
     * @param VariableStmt $statement
     */
    public function visitVariableStmt(VariableStmt $statement): void
    {
        $name = $statement->getName()->getLexeme();
        $value = null;

        if ($statement->getInitializer()) {
            $value = $this->evaluate($statement->getInitializer());
        }

        $this->environment->define($name, $value);
    }

    /**
     * @param BlockStmt $statement
     */
    public function visitBlockStmt(BlockStmt $statement): void
    {
        $this->executeBlock($statement->getStatements(), new Environment($this->environment));
    }

    /**
     * @param IfStmt $statement
     */
    public function visitIfStmt(IfStmt $statement): void
    {
        $condition = $this->evaluate($statement->getCondition());

        if ($this->isTruthy($condition)) {
            $this->execute($statement->getThenBranch());
        } elseif ($statement->getElseBranch()) {
            $this->execute($statement->getElseBranch());
        }
    }

    /**
     * @param WhileStmt $statement
     */
    public function visitWhileStmt(WhileStmt $statement): void
    {
        while ($this->isTruthy($this->evaluate($statement->getCondition()))) {
            $this->execute($statement->getBody());
        }
    }

    /**
     * @param FunctionStmt $statement
     */
    public function visitFunctionStmt(FunctionStmt $statement): void
    {
        $this->environment->define($statement->getName()->getLexeme(), new LoxFunction($statement, $this->environment));
    }

    /**
     * @param ReturnStmt $statement
     * @throws ReturnValue
     */
    public function visitReturnStmt(ReturnStmt $statement): void
    {
        $value = $statement->getValue() ? $this->evaluate($statement->getValue()) : null;

        throw new ReturnValue($value);
    }

    /**
     * @param Binary $binary
     * @return string
     * @throws \Exception
     */
    public function visitBinary(Binary $binary)
    {
        $left = $this->evaluate($binary->getLeft());
        $right = $this->evaluate($binary->getRight());

        switch ($binary->getOperator()->getType()) {
            case TokenType::T_PLUS:
                if (is_float($left) && is_float($right)) {
                    return $left + $right;
                }
                if (is_string($left) || is_string($right)) {
                    return $left . $right;
                }

                throw new \Exception('Left and right of a binary should either be a string or number');

            case TokenType::T_MINUS:
                $this->validateNumbers($left, $right);
                return $left - $right;

            case TokenType::T_SLASH:
                $this->validateNumbers($left, $right);
                return $left / $right;

            case TokenType::T_STAR:
                $this->validateNumbers($left, $right);
                return $left * $right;

            case TokenType::T_GREATER:
                $this->validateNumbers($left, $right);
                return $left > $right;

            case TokenType::T_GREATER_EQUAL:
                $this->validateNumbers($left, $right);
                return $left >= $right;

            case TokenType::T_LESS:
                $this->validateNumbers($left, $right);
                return $left < $right;

            case TokenType::T_LESS_EQUAL:
                $this->validateNumbers($left, $right);
                return $left <= $right;
        }
    }

    private function validateNumbers($left, $right)
    {
        if (!is_float($left) || !is_float($right)) {
            throw new \Exception('Left and right of a binary should both be numbers');
        }
    }

    /**
     * @param Grouping $grouping
     * @return mixed
     */
    public function visitGrouping(Grouping $grouping)
    {
        return $this->evaluate($grouping->getExpression());
    }

    /**
     * @param Literal $literal
     * @return mixed
     */
    public function visitLiteral(Literal $literal)
    {
        return $literal->getValue();
    }

    /**
     * @param Unary $unary
     * @return mixed
     * @throws \Exception
     */
    public function visitUnary(Unary $unary)
    {
        $right = $this->evaluate($unary->getRight());

        switch ($unary->getOperator()->getType()) {
            case TokenType::T_MINUS:
                if (!is_float($right)) {
                    throw new \Exception('The right of a unary minus should be a number');
                }

                return -$right;

            case TokenType::T_BANG:
                return !$this->isTruthy($right);
        }

        throw new \Exception('This should never be reached?');
    }

    /**
     * @param Variable $variable
     * @return mixed
     */
    public function visitVariable(Variable $variable)
    {
        $name = $variable->getName()->getLexeme();

        return $this->environment->get($name);
    }

    /**
     * @param Assign $assign
     * @return mixed
     */
    public function visitAssign(Assign $assign)
    {
        $name = $assign->getName()->getLexeme();
        $value = $this->evaluate($assign->getValue());

        $this->environment->assign($name, $value);
    }

    /**
     * @param Logical $logical
     * @return mixed
     */
    public function visitLogical(Logical $logical)
    {
        $left = $this->evaluate($logical->getLeft());

        if ($logical->getOperator()->getType() === TokenType::T_OR) {
            if ($this->isTruthy($left)) {
                return $left;
            }
        } else {
            if (!$this->isTruthy($left)) {
                return $left;
            }
        }

        return $this->evaluate($logical->getRight());
    }

    /**
     * @param Call $call
     * @return mixed
     */
    public function visitCall(Call $call)
    {
        $callee = $this->evaluate($call->getCallee());

        if (!($callee instanceof LoxCallable)) {
            throw new \RuntimeException('Can only call functions and classes');
        }
        if (count($call->getArguments()) !== $callee->arity()) {
            throw new \RuntimeException(
                sprintf('Expected %u arguments but got %u', $callee->arity(), count($call->getArguments()))
            );
        }

        $arguments = [];

        foreach ($call->getArguments() as $argument) {
            $arguments[] = $this->evaluate($argument);
        }

        return $callee->call($this, $arguments);
    }

    /**
     * @param array $statements
     * @param Environment $environment
     * @throws \RuntimeException|ReturnValue
     */
    public function executeBlock(array $statements, Environment $environment)
    {
        $previous = $this->environment;

        try {
            $this->environment = $environment;

            foreach ($statements as $statement) {
                $this->execute($statement);
            }
        } finally {
            $this->environment = $previous;
        }
    }

    /**
     * @param mixed $value
     * @return bool
     */
    private function isTruthy($value): bool
    {
        if (is_bool($value)) {
            return $value;
        }
        if (is_null($value)) {
            return false;
        }

        return true;
    }

    /**
     * @param mixed $value
     * @return string
     */
    private function stringify($value): string
    {
        if (is_bool($value)) {
            return $value ? 'true' : 'false';
        }
        if (is_null($value)) {
            return 'nil';
        }

        return (string) $value;
    }

    /**
     * @return Environment
     */
    public function getGlobals(): Environment
    {
        return $this->globals;
    }
}
<?php
namespace Plox\STL;

use Plox\Environment;

abstract class Factory
{
    /**
     * @return Environment
     */
    public static function create(): Environment
    {
        $environment = new Environment();

        $environment->define('loxinfo', new LoxInfo());

        return $environment;
    }
}
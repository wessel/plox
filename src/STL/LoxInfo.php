<?php
namespace Plox\STL;

use Plox\Interpreter;
use Plox\LoxCallable;

class LoxInfo implements LoxCallable
{
    /**
     * @return int
     */
    public function arity(): int
    {
        return 0;
    }

    /**
     * @param Interpreter $interpreter
     * @param mixed[] $arguments
     * @return mixed
     */
    public function call(Interpreter $interpreter, array $arguments)
    {
        return "Lox interpreter written in PHP (because why the fuck not)";
    }
}
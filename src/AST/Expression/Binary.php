<?php
namespace Plox\AST\Expression;

use Plox\Token;

class Binary extends Expression
{
    /**
     * @var Expression
     */
    private $left;

    /**
     * @var Expression
     */
    private $right;

    /**
     * @var Token
     */
    private $operator;

    /**
     * @param Expression $left
     * @param Expression $right
     * @param Token $operator
     */
    public function __construct(Expression $left, Expression $right, Token $operator)
    {
        $this->left = $left;
        $this->right = $right;
        $this->operator = $operator;
    }

    /**
     * @param ExpressionVisitor $visitor
     * @return mixed
     */
    public function accept(ExpressionVisitor $visitor)
    {
        return $visitor->visitBinary($this);
    }

    /**
     * @return Expression
     */
    public function getLeft(): Expression
    {
        return $this->left;
    }

    /**
     * @return Expression
     */
    public function getRight(): Expression
    {
        return $this->right;
    }

    /**
     * @return Token
     */
    public function getOperator(): Token
    {
        return $this->operator;
    }
}
<?php
namespace Plox\AST\Expression;

use Plox\Token;

class Logical extends Expression
{
    /**
     * @var Expression
     */
    private $left;

    /**
     * @var Token
     */
    private $operator;

    /**
     * @var Expression
     */
    private $right;

    /**
     * @param Expression $left
     * @param Token $operator
     * @param Expression $right
     */
    public function __construct(Expression $left, Token $operator, Expression $right)
    {
        $this->left = $left;
        $this->operator = $operator;
        $this->right = $right;
    }

    /**
     * @param ExpressionVisitor $visitor
     * @return mixed
     */
    public function accept(ExpressionVisitor $visitor)
    {
        return $visitor->visitLogical($this);
    }

    /**
     * @return Expression
     */
    public function getLeft(): Expression
    {
        return $this->left;
    }

    /**
     * @return Token
     */
    public function getOperator(): Token
    {
        return $this->operator;
    }

    /**
     * @return Expression
     */
    public function getRight(): Expression
    {
        return $this->right;
    }
}
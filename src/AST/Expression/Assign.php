<?php
namespace Plox\AST\Expression;

use Plox\Token;

class Assign extends Expression
{
    /**
     * @var Token
     */
    private $name;

    /**
     * @var Expression
     */
    private $value;

    /**
     * @param Token $name
     * @param Expression $value
     */
    public function __construct(Token $name, Expression $value)
    {
        $this->name = $name;
        $this->value = $value;
    }

    /**
     * @param ExpressionVisitor $visitor
     * @return mixed
     */
    public function accept(ExpressionVisitor $visitor)
    {
        return $visitor->visitAssign($this);
    }

    /**
     * @return Token
     */
    public function getName(): Token
    {
        return $this->name;
    }

    /**
     * @return Expression
     */
    public function getValue(): Expression
    {
        return $this->value;
    }
}
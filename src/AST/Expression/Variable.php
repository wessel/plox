<?php
namespace Plox\AST\Expression;

use Plox\Token;

class Variable extends Expression
{
    /**
     * @var Token
     */
    private $name;

    /**
     * @param Token $name
     */
    public function __construct(Token $name)
    {
        $this->name = $name;
    }

    /**
     * @param ExpressionVisitor $visitor
     * @return mixed
     */
    public function accept(ExpressionVisitor $visitor)
    {
        return $visitor->visitVariable($this);
    }

    /**
     * @return Token
     */
    public function getName(): Token
    {
        return $this->name;
    }
}
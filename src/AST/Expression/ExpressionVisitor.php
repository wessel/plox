<?php
namespace Plox\AST\Expression;

interface ExpressionVisitor
{
    /**
     * @param Binary $binary
     * @return mixed
     */
    public function visitBinary(Binary $binary);

    /**
     * @param Grouping $grouping
     * @return mixed
     */
    public function visitGrouping(Grouping $grouping);

    /**
     * @param Literal $literal
     * @return mixed
     */
    public function visitLiteral(Literal $literal);

    /**
     * @param Unary $unary
     * @return mixed
     */
    public function visitUnary(Unary $unary);

    /**
     * @param Variable $variable
     * @return mixed
     */
    public function visitVariable(Variable $variable);

    /**
     * @param Assign $assign
     * @return mixed
     */
    public function visitAssign(Assign $assign);

    /**
     * @param Logical $logical
     * @return mixed
     */
    public function visitLogical(Logical $logical);

    /**\
     * @param Call $call
     * @return mixed
     */
    public function visitCall(Call $call);
}
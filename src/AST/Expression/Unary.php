<?php
namespace Plox\AST\Expression;

use Plox\Token;

class Unary extends Expression
{
    /**
     * @var Token
     */
    private $operator;

    /**
     * @var Expression
     */
    private $right;

    /**
     * @param Token $operator
     * @param Expression $right
     */
    public function __construct(Token $operator, Expression $right)
    {
        $this->operator = $operator;
        $this->right = $right;
    }

    /**
     * @param ExpressionVisitor $visitor
     * @return mixed
     */
    public function accept(ExpressionVisitor $visitor)
    {
        return $visitor->visitUnary($this);
    }

    /**
     * @return Token
     */
    public function getOperator(): Token
    {
        return $this->operator;
    }

    /**
     * @return Expression
     */
    public function getRight(): Expression
    {
        return $this->right;
    }
}
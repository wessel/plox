<?php
namespace Plox\AST\Expression;

abstract class Expression
{
    /**
     * @param ExpressionVisitor $visitor
     * @return mixed
     */
    abstract public function accept(ExpressionVisitor $visitor);
}
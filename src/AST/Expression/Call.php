<?php
namespace Plox\AST\Expression;

class Call extends Expression
{
    /**
     * @var Expression
     */
    private $callee;

    /**
     * @var Expression[]
     */
    private $arguments;

    /**
     * @param Expression $callee
     * @param Expression[] $arguments
     */
    public function __construct(Expression $callee, array $arguments)
    {
        $this->callee = $callee;
        $this->arguments = $arguments;
    }

    /**
     * @param ExpressionVisitor $visitor
     * @return mixed
     */
    public function accept(ExpressionVisitor $visitor)
    {
        return $visitor->visitCall($this);
    }

    /**
     * @return Expression
     */
    public function getCallee(): Expression
    {
        return $this->callee;
    }

    /**
     * @return Expression[]
     */
    public function getArguments(): array
    {
        return $this->arguments;
    }
}
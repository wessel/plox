<?php
namespace Plox\AST\Expression;

class Literal extends Expression
{
    /**
     * @var mixed
     */
    private $value;

    /**
     * @param mixed $value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * @param ExpressionVisitor $visitor
     * @return mixed
     */
    public function accept(ExpressionVisitor $visitor)
    {
        return $visitor->visitLiteral($this);
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }
}
<?php
namespace Plox\AST\Statement;

use Plox\AST\Expression\Expression;

class WhileStmt extends Statement
{
    /**
     * @var Expression
     */
    private $condition;

    /**
     * @var Statement
     */
    private $body;

    /**
     * @param Expression $expression
     * @param Statement $body
     */
    public function __construct(Expression $expression, Statement $body)
    {
        $this->condition = $expression;
        $this->body = $body;
    }

    /**
     * @param StatementVisitor $visitor
     */
    public function accept(StatementVisitor $visitor)
    {
        $visitor->visitWhileStmt($this);
    }

    /**
     * @return Expression
     */
    public function getCondition(): Expression
    {
        return $this->condition;
    }

    /**
     * @return Statement
     */
    public function getBody(): Statement
    {
        return $this->body;
    }
}
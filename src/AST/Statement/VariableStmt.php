<?php
namespace Plox\AST\Statement;

use Plox\AST\Expression\Expression;
use Plox\Token;

class VariableStmt extends Statement
{
    /**
     * @var Token
     */
    private $name;

    /**
     * @var Expression|null
     */
    private $initializer;

    /**
     * @param Token $name
     * @param Expression|null $initializer
     */
    public function __construct(Token $name, ?Expression $initializer)
    {
        $this->name = $name;
        $this->initializer = $initializer;
    }

    /**
     * @param StatementVisitor $visitor
     */
    public function accept(StatementVisitor $visitor)
    {
        $visitor->visitVariableStmt($this);
    }

    /**
     * @return Token
     */
    public function getName(): Token
    {
        return $this->name;
    }

    /**
     * @return Expression|null
     */
    public function getInitializer(): ?Expression
    {
        return $this->initializer;
    }
}
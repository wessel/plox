<?php
namespace Plox\AST\Statement;

interface StatementVisitor
{
    /**
     * @param ExpressionStmt $statement
     */
    public function visitExpressionStmt(ExpressionStmt $statement): void;

    /**
     * @param PrintStmt $statement
     */
    public function visitPrintStmt(PrintStmt $statement): void;

    /**
     * @param VariableStmt $statement
     */
    public function visitVariableStmt(VariableStmt $statement): void;

    /**
     * @param BlockStmt $statement
     */
    public function visitBlockStmt(BlockStmt $statement): void;

    /**
     * @param IfStmt $statement
     */
    public function visitIfStmt(IfStmt $statement): void;

    /**
     * @param WhileStmt $statement
     */
    public function visitWhileStmt(WhileStmt $statement): void;

    /**
     * @param FunctionStmt $statement
     */
    public function visitFunctionStmt(FunctionStmt $statement): void;

    /**
     * @param ReturnStmt $statement
     */
    public function visitReturnStmt(ReturnStmt $statement): void;
}
<?php
namespace Plox\AST\Statement;

use Plox\Token;

class FunctionStmt extends Statement
{
    /**
     * @var Token
     */
    private $name;

    /**
     * @var Token[]
     */
    private $params;

    /**
     * @var Statement[]
     */
    private $body;

    /**
     * @param Token $name
     * @param Token[] $params
     * @param Statement[] $body
     */
    public function __construct(Token $name, array $params, array $body)
    {
        $this->name = $name;
        $this->params = $params;
        $this->body = $body;
    }

    /**
     * @param StatementVisitor $visitor
     */
    public function accept(StatementVisitor $visitor)
    {
        $visitor->visitFunctionStmt($this);
    }

    /**
     * @return Token
     */
    public function getName(): Token
    {
        return $this->name;
    }

    /**
     * @return Token[]
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * @return Statement[]
     */
    public function getBody(): array
    {
        return $this->body;
    }
}
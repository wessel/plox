<?php
namespace Plox\AST\Statement;

use Plox\AST\Expression\Expression;

class IfStmt extends Statement
{
    /**
     * @var Expression
     */
    private $condition;

    /**
     * @var Statement
     */
    private $thenBranch;

    /**
     * @var Statement|null
     */
    private $elseBranch;

    /**
     * @param Expression $condition
     * @param Statement $thenBranch
     * @param Statement|null $elseBranch
     */
    public function __construct(Expression $condition, Statement $thenBranch, Statement $elseBranch = null)
    {
        $this->condition = $condition;
        $this->thenBranch = $thenBranch;
        $this->elseBranch = $elseBranch;
    }

    /**
     * @param StatementVisitor $visitor
     */
    public function accept(StatementVisitor $visitor)
    {
        $visitor->visitIfStmt($this);
    }

    /**
     * @return Expression
     */
    public function getCondition(): Expression
    {
        return $this->condition;
    }

    /**
     * @return Statement
     */
    public function getThenBranch(): Statement
    {
        return $this->thenBranch;
    }

    /**
     * @return Statement|null
     */
    public function getElseBranch(): ?Statement
    {
        return $this->elseBranch;
    }
}
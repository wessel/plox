<?php
namespace Plox\AST\Statement;

abstract class Statement
{
    abstract public function accept(StatementVisitor $visitor);
}
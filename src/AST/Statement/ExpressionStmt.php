<?php
namespace Plox\AST\Statement;

use Plox\AST\Expression\Expression;

class ExpressionStmt extends Statement
{
    /**
     * @var Expression
     */
    private $expression;

    /**
     * @param Expression $expression
     */
    public function __construct(Expression $expression)
    {
        $this->expression = $expression;
    }

    /**
     * @param StatementVisitor $visitor
     */
    public function accept(StatementVisitor $visitor)
    {
        $visitor->visitExpressionStmt($this);
    }

    /**
     * @return Expression
     */
    public function getExpression(): Expression
    {
        return $this->expression;
    }
}
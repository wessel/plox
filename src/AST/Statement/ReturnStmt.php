<?php
namespace Plox\AST\Statement;

use Plox\AST\Expression\Expression;
use Plox\Token;

class ReturnStmt extends Statement
{
    /**
     * @var Token
     */
    private $keyword;

    /**
     * @var Expression|null
     */
    private $value;

    /**
     * @param Token $keyword
     * @param Expression|null $value
     */
    public function __construct(Token $keyword, Expression $value = null)
    {
        $this->keyword = $keyword;
        $this->value = $value;
    }

    /**
     * @param StatementVisitor $visitor
     */
    public function accept(StatementVisitor $visitor)
    {
        $visitor->visitReturnStmt($this);
    }

    /**
     * @return Token
     */
    public function getKeyword(): Token
    {
        return $this->keyword;
    }

    /**
     * @return Expression|null
     */
    public function getValue(): ?Expression
    {
        return $this->value;
    }
}
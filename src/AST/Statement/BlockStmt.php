<?php
namespace Plox\AST\Statement;

class BlockStmt extends Statement
{
    /**
     * @var Statement[]
     */
    private $statements;

    /**
     * @param Statement[] $statements
     */
    public function __construct(array $statements)
    {
        $this->statements = $statements;
    }

    /**
     * @param StatementVisitor $visitor
     */
    public function accept(StatementVisitor $visitor)
    {
        $visitor->visitBlockStmt($this);
    }

    /**
     * @return Statement[]
     */
    public function getStatements(): array
    {
        return $this->statements;
    }
}
<?php
namespace Plox\AST\Statement;

use Plox\AST\Expression\Expression;

class PrintStmt extends Statement
{
    /**
     * @var Expression
     */
    private $expression;

    /**
     * @param Expression $expression
     */
    public function __construct(Expression $expression)
    {
        $this->expression = $expression;
    }

    /**
     * @param StatementVisitor $visitor
     */
    public function accept(StatementVisitor $visitor)
    {
        $visitor->visitPrintStmt($this);
    }

    /**
     * @return Expression
     */
    public function getExpression(): Expression
    {
        return $this->expression;
    }
}
<?php
namespace Plox;

class Environment
{
    /**
     * @var array
     */
    private $values;

    /**
     * @var Environment|null
     */
    private $enclosing;

    public function __construct(Environment $enclosing = null)
    {
        $this->values = [];
        $this->enclosing = $enclosing;
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function define(string $name, $value): void
    {
        if ($this->has($name)) {
            throw new \RuntimeException(sprintf("Variable '%s' is already defined", $name));
        }

        $this->values[$name] = $value;
    }

    /**
     * @param string $name
     * @param $value
     */
    public function assign(string $name, $value): void
    {
        if ($this->has($name)) {
            $this->values[$name] = $value;
        } elseif ($this->enclosing) {
            $this->enclosing->assign($name, $value);
        } else {
            throw new \RuntimeException(sprintf("Variable '%s' is not defined", $name));
        }
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function get(string $name)
    {
        if ($this->has($name)) {
            return $this->values[$name];
        }

        if ($this->enclosing) {
            return $this->enclosing->get($name);
        }

        throw new \RuntimeException(sprintf("Undefined variable '%s'", $name));
    }

    /**
     * @param string $name
     * @return bool
     */
    protected function has(string $name): bool
    {
        if (array_key_exists($name, $this->values)) {
            return true;
        }

        return false;
    }
}
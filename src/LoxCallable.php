<?php
namespace Plox;

interface LoxCallable
{
    /**
     * @return int
     */
    public function arity(): int;

    /**
     * @param Interpreter $interpreter
     * @param mixed[] $arguments
     * @return mixed
     */
    public function call(Interpreter $interpreter, array $arguments);
}
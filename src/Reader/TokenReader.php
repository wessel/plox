<?php
namespace Plox\Reader;

use Plox\Token;
use Plox\TokenType;

class TokenReader
{
    /**
     * @var Token[]
     */
    private $tokens;

    /**
     * @var int
     */
    private $cursor;

    /**
     * @param Token[] $tokens
     */
    public function __construct(array $tokens)
    {
        $this->tokens = $tokens;
        $this->cursor = 0;
    }

    /**
     * @return Token|null
     */
    public function advance(): Token
    {
        if ($this->isAtEnd()) {
            return $this->createEofToken();
        }

        return $this->tokens[$this->cursor++];
    }

    /**
     * @return Token
     */
    public function peek(): Token
    {
        if ($this->isAtEnd()) {
            return $this->createEofToken();
        }

        return $this->tokens[$this->cursor];
    }

    /**
     * @param string ...$types
     * @return bool
     */
    public function check(string ...$types): bool
    {
        foreach ($types as $type) {
            if ($this->peek()->getType() === $type) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $type
     * @return bool
     */
    public function match(string $type): bool
    {
        if (!$this->check($type)) {
            return false;
        }

        $this->advance();

        return true;
    }

    /**
     * @return bool
     */
    public function isAtEnd(): bool
    {
        if ($this->cursor >= count($this->tokens)) {
            return true;
        }

        $current = $this->tokens[$this->cursor];

        return $current->getType() === TokenType::T_EOF;
    }

    /**
     * @return int
     */
    public function getCursor(): int
    {
        return $this->cursor;
    }

    /**
     * @return Token
     */
    private function createEofToken(): Token
    {
        return new Token(TokenType::T_EOF, '', null, -1);
    }
}
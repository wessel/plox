<?php
namespace Plox\Reader;

class StringReader
{
    /**
     * @var string
     */
    private $source;

    /**
     * @var int
     */
    private $cursor;

    /**
     * Start of the current selection
     *
     * @var int
     */
    private $start;

    /**
     * @param string $source
     */
    public function __construct(string $source)
    {
        $this->source = $source;
        $this->cursor = 0;
        $this->start = -1;
    }

    /**
     * Returns the current character and advances the cursor by one
     *
     * @return string|null
     */
    public function advance(): ?string
    {
        if ($this->isAtEnd()) {
            return null;
        }

        return $this->source[$this->cursor++];
    }

    /**
     * @return string|null
     */
    public function peek(): ?string
    {
        if ($this->isAtEnd()) {
            return null;
        }

        return $this->source[$this->cursor];
    }

    /**
     * @param string $expected
     * @return bool
     */
    public function match(string $expected): bool
    {
        if ($this->peek() !== $expected) {
            return false;
        }

        $this->advance();

        return true;
    }

    /**
     * @return bool
     */
    public function isAtEnd(): bool
    {
        return $this->cursor >= strlen($this->source);
    }

    public function startSelection(): void
    {
        $this->start = $this->cursor;
    }

    /**
     * @return string
     */
    public function endSelection(): string
    {
        if ($this->start === -1) {
            throw new \RuntimeException('No selectionhas been started yet');
        }

        $selection = substr($this->source, $this->start, $this->cursor - $this->start);

        return $selection;
    }

    public function getStart()
    {
        return $this->start;
    }


    /**
     * @return int
     */
    public function getCursor(): int
    {
        return $this->cursor;
    }
}
<?php
namespace Plox;

use Plox\AST\Expression\Assign;
use Plox\AST\Expression\Binary;
use Plox\AST\Expression\Call;
use Plox\AST\Expression\Expression;
use Plox\AST\Expression\Grouping;
use Plox\AST\Expression\Literal;
use Plox\AST\Expression\Logical;
use Plox\AST\Expression\Unary;
use Plox\AST\Expression\Variable;
use Plox\AST\Statement\BlockStmt;
use Plox\AST\Statement\ExpressionStmt;
use Plox\AST\Statement\FunctionStmt;
use Plox\AST\Statement\IfStmt;
use Plox\AST\Statement\PrintStmt;
use Plox\AST\Statement\ReturnStmt;
use Plox\AST\Statement\Statement;
use Plox\AST\Statement\VariableStmt;
use Plox\AST\Statement\WhileStmt;
use Plox\Reader\TokenReader;

class Parser
{
    /**
     * @var TokenReader
     */
    private $reader;

    /**
     * @param TokenReader $reader
     */
    public function __construct(TokenReader $reader)
    {
        $this->reader = $reader;
    }

    /**
     * @return Statement[]
     */
    public function parse(): array
    {
        $statements = [];

        while (!$this->reader->isAtEnd()) {
            $statements[] = $this->declaration();
        }

        return $statements;
    }

    private function declaration(): Statement
    {
        if ($this->reader->match(TokenType::T_FUN)) {
            return $this->functionDeclaration();
        }

        if ($this->reader->match(TokenType::T_VAR)) {
            return $this->varDeclaration();
        }

        return $this->statement();
    }

    private function functionDeclaration(): Statement
    {
        $name = $this->consume(TokenType::T_IDENTIFIER, 'Expect identifier in function declaration');

        $this->consume(TokenType::T_LEFT_PAREN, "Expect '(' after function name");

        $parameters = [];

        if (!$this->reader->check(TokenType::T_RIGHT_PAREN)) {
            do {
                $parameters[] = $this->consume(TokenType::T_IDENTIFIER, 'Expect parameter name');
            } while ($this->reader->match(TokenType::T_COMMA));
        }

        $this->consume(TokenType::T_RIGHT_PAREN, "Expect ')' after function parameters");

        /** @var BlockStmt $body */
        $body = $this->block();

        return new FunctionStmt($name, $parameters, $body->getStatements());
    }

    private function varDeclaration(): Statement
    {
        if (!$this->reader->check(TokenType::T_IDENTIFIER)) {
            throw new \Exception('Expect identifier after "var"');
        }

        $name = $this->reader->advance();
        $initializer = null;

        if ($this->reader->match(TokenType::T_EQUAL)) {
            $initializer = $this->expression();
        }

        $this->consume(TokenType::T_SEMICOLON, 'Expect ";" after var declaration');

        return new VariableStmt($name, $initializer);
    }

    private function statement(): Statement
    {
        switch (true) {
            case $this->reader->match(TokenType::T_PRINT):
                return $this->printStatement();

            case $this->reader->match(TokenType::T_IF):
                return $this->ifStatement();

            case $this->reader->check(TokenType::T_RETURN):
                return $this->returnStatement();

            case $this->reader->match(TokenType::T_WHILE):
                return $this->whileStatement();

            case $this->reader->check(TokenType::T_LEFT_BRACE):
                return $this->block();
        }

        return $this->expressionStatement();
    }

    private function expressionStatement(): Statement
    {
        $expression = $this->expression();

        $this->consume(TokenType::T_SEMICOLON, 'Expect ";" after expression statement');

        return new ExpressionStmt($expression);
    }

    private function printStatement(): Statement
    {
        $expression = $this->expression();

        $this->consume(TokenType::T_SEMICOLON, 'Expect ";" after print statement');

        return new PrintStmt($expression);
    }

    private function block(): Statement
    {
        $this->consume(TokenType::T_LEFT_BRACE, "Expect '{' before block statement");

        $statements = [];

        while (!$this->reader->check(TokenType::T_RIGHT_BRACE) && !$this->reader->isAtEnd()) {
            $statements[] = $this->declaration();
        }

        $this->consume(TokenType::T_RIGHT_BRACE, "Expect '}' after block statement");

        return new BlockStmt($statements);
    }

    private function ifStatement(): Statement
    {
        $this->consume(TokenType::T_LEFT_PAREN, "Expect '(' before if expression");
        $condition = $this->expression();
        $this->consume(TokenType::T_RIGHT_PAREN, "Expect ')' after if expression");

        $thenBranch = $this->statement();
        $elseBranch = null;

        if ($this->reader->match(TokenType::T_ELSE)) {
            $elseBranch = $this->statement();
        }

        return new IfStmt($condition, $thenBranch, $elseBranch);
    }

    private function returnStatement(): Statement
    {
        $keyword = $this->consume(TokenType::T_RETURN, 'Expect return in return statement');
        $value = null;

        if (!$this->reader->check(TokenType::T_SEMICOLON)) {
            $value = $this->expression();
        }

        $this->consume(TokenType::T_SEMICOLON, "Expect ';' after return statement");

        return new ReturnStmt($keyword, $value);
    }

    private function whileStatement(): Statement
    {
        $this->consume(TokenType::T_LEFT_PAREN, "Expect '(' before while expression");
        $condition = $this->expression();
        $this->consume(TokenType::T_RIGHT_PAREN, "Expect ')' after while expression");
        $body = $this->statement();

        return new WhileStmt($condition, $body);
    }

    public function expression(): Expression
    {
        return $this->assignment();
    }

    private function assignment(): Expression
    {
        $expression = $this->logicalOr();

        if ($this->reader->match(TokenType::T_EQUAL)) {
            $value = $this->assignment();

            if (!($expression instanceof Variable)) {
                throw new \Exception('Invalid assignment target');
            }

            $name = $expression->getName();

            return new Assign($name, $value);
        }

        return $expression;
    }

    private function logicalOr(): Expression
    {
        $expression = $this->logicalAnd();

        while ($this->reader->check(TokenType::T_OR)) {
            $operator = $this->reader->advance();
            $right = $this->logicalOr();
            $expression = new Logical($expression, $operator, $right);
        }

        return $expression;
    }

    private function logicalAnd(): Expression
    {
        $expression = $this->equality();

        while ($this->reader->check(TokenType::T_AND)) {
            $operator = $this->reader->advance();
            $right = $this->logicalAnd();
            $expression = new Logical($expression, $operator, $right);
        }

        return $expression;
    }

    private function equality(): Expression
    {
        $expression = $this->comparison();

        while ($this->reader->check(TokenType::T_BANG_EQUAL, TokenType::T_EQUAL_EQUAL)) {
            $operator = $this->reader->advance();
            $right = $this->comparison();
            $expression = new Binary($expression, $right, $operator);
        }

        return $expression;
    }

    private function comparison(): Expression
    {
        $expression = $this->addition();

        while ($this->reader->check(TokenType::T_GREATER, TokenType::T_GREATER_EQUAL, TokenType::T_LESS, TokenType::T_LESS_EQUAL)) {
            $operator = $this->reader->advance();
            $right = $this->addition();
            $expression = new Binary($expression, $right, $operator);
        }

        return $expression;
    }

    private function addition(): Expression
    {
        $expression = $this->multiplication();

        while ($this->reader->check(TokenType::T_MINUS, TokenType::T_PLUS)) {
            $operator = $this->reader->advance();
            $right = $this->multiplication();
            $expression = new Binary($expression, $right, $operator);
        }

        return $expression;
    }

    private function multiplication(): Expression
    {
        $expression = $this->unary();

        while ($this->reader->check(TokenType::T_SLASH, TokenType::T_STAR)) {
            $operator = $this->reader->advance();
            $right = $this->multiplication();
            $expression = new Binary($expression, $right, $operator);
        }

        return $expression;
    }

    private function unary(): Expression
    {
        if ($this->reader->check(TokenType::T_BANG, TokenType::T_MINUS)) {
            $token = $this->reader->advance();
            $operator = $this->unary();

            return new Unary($token, $operator);
        }

        return $this->call();
    }

    private function call(): Expression
    {
        $expression = $this->primary();

        while ($this->reader->match(TokenType::T_LEFT_PAREN)) {
            $expression = $this->finishCall($expression);
        }

        return $expression;
    }

    private function finishCall(Expression $callee): Expression
    {
        $arguments = [];

        if (!$this->reader->check(TokenType::T_RIGHT_PAREN)) {
            do {
                $arguments[] = $this->expression();
            } while ($this->reader->match(TokenType::T_COMMA));
        }

        $this->consume(TokenType::T_RIGHT_PAREN, "Expect ')' after function call");

        return new Call($callee, $arguments);
    }

    private function primary(): Expression
    {
        switch (true) {
            case $this->reader->match(TokenType::T_FALSE):
                return new Literal(false);

            case $this->reader->match(TokenType::T_TRUE):
                return new Literal(true);

            case $this->reader->match(TokenType::T_NIL):
                return new Literal(null);

            case $this->reader->check(TokenType::T_STRING):
                return new Literal($this->reader->advance()->getLiteral());

            case $this->reader->check(TokenType::T_NUMBER):
                return new Literal((float) $this->reader->advance()->getLiteral());

            case $this->reader->match(TokenType::T_LEFT_PAREN):
                $expression = $this->expression();

                $this->consume(TokenType::T_RIGHT_PAREN, 'Expect ")" after group expression');

                return new Grouping($expression);

            case $this->reader->check(TokenType::T_IDENTIFIER):
                return new Variable($this->reader->advance());
        }

        throw new \Exception('Expect expression');
    }

    private function consume(string $type, string $errorMessage): Token
    {
        if (!$this->reader->check($type)) {
            throw new \Exception($errorMessage);
        }

        return $this->reader->advance();
    }
}
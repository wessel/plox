<?php
namespace Plox;

use PHPUnit\Framework\TestCase;
use Plox\AST\Statement\FunctionStmt;
use Plox\Exception\Interpreter\ReturnValue;

class LoxFunctionTest extends TestCase
{
    public function arityProvider(): array
    {
        return [
            [
                new FunctionStmt(
                    new Token(TokenType::T_IDENTIFIER, 'test', null, 0),
                    [
                    ],
                    []
                ),
                0
            ],
            [
                new FunctionStmt(
                    new Token(TokenType::T_IDENTIFIER, 'test', null, 0),
                    [
                        new Token(TokenType::T_IDENTIFIER, 'param1', null, 0),
                    ],
                    []
                ),
                1
            ],
            [
                new FunctionStmt(
                    new Token(TokenType::T_IDENTIFIER, 'test', null, 0),
                    [
                        new Token(TokenType::T_IDENTIFIER, 'param1', null, 0),
                        new Token(TokenType::T_IDENTIFIER, 'param2', null, 0),
                    ],
                    []
                ),
                2
            ]
        ];
    }

    /**
     * @dataProvider arityProvider
     * @param FunctionStmt $declaration
     * @param int $expected
     */
    public function testArity(FunctionStmt $declaration, int $expected)
    {
        $function = new LoxFunction($declaration, new Environment());

        $this->assertEquals($expected, $function->arity());
    }

    public function callProvider(): array
    {
        return [
            'with parameters' => [
                'declaration' => new FunctionStmt(
                    new Token(TokenType::T_IDENTIFIER, 'test', null, 0),
                    [
                        new Token(TokenType::T_IDENTIFIER, 'param1', null, 0),
                        new Token(TokenType::T_IDENTIFIER, 'param2', null, 0),
                    ],
                    []
                ),
                'executeBlock' => function (array $statements, Environment $environment) {
                    $expected = new Environment(new Environment());
                    $expected->define('param1', 1337);
                    $expected->define('param2', 'something');

                    $this->assertEquals([], $statements);
                    $this->assertEquals($expected, $environment);
                },
                'arguments' => [1337, 'something'],
                'expectedReturnValue' => null,
            ],
            'return value' => [
                'declaration' => new FunctionStmt(
                    new Token(TokenType::T_IDENTIFIER, 'test', null, 0),
                    [],
                    []
                ),
                'executeBlock' => function (array $statements, Environment $environment) {
                    throw new ReturnValue('success');
                },
                'arguments' => [],
                'expectedReturnValue' => 'success',
            ]
        ];
    }

    /**
     * @dataProvider callProvider
     * @param FunctionStmt $declaration
     * @param callable $executeBlock
     * @param array $arguments
     * @param mixed $expectedReturnValue
     * @throws \ReflectionException
     */
    public function testCall(FunctionStmt $declaration, callable $executeBlock, array $arguments, $expectedReturnValue)
    {
        $function = new LoxFunction($declaration, new Environment());
        $interpreter = $this->createMock(Interpreter::class);

        $interpreter->method('getGlobals')->willReturn(new Environment());

        $interpreter
            ->expects($this->once())
            ->method('executeBlock')
            ->willReturnCallback($executeBlock);

        $returnValue = $function->call($interpreter, $arguments);

        $this->assertEquals($expectedReturnValue, $returnValue);
    }
}
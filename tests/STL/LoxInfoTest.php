<?php
namespace PloxTest\STL;

use PHPUnit\Framework\TestCase;
use Plox\Interpreter;
use Plox\STL\LoxInfo;

class LoxInfoTest extends TestCase
{
    public function testArity()
    {
        $func = new LoxInfo();
        $this->assertEquals(0, $func->arity());
    }

    public function testCall()
    {
        $interpreter = $this->createMock(Interpreter::class);

        $func = new LoxInfo();
        $result = $func->call($interpreter, []);

        $this->assertEquals("Lox interpreter written in PHP (because why the fuck not)", $result);
    }
}
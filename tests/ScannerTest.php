<?php
namespace Plox;

use PHPUnit\Framework\TestCase;
use Plox\Reader\StringReader;

class ScannerTest extends TestCase
{
    public function scanTokensProvider(): array
    {
        return [
            'ends with T_EOF' => [
                '',
                [
                    new Token(TokenType::T_EOF, '', null, 0),
                ]
            ],
            'ignores whitespace' => [
                " \n\r\t",
                [
                    new Token(TokenType::T_EOF, '', null, 4),
                ]
            ],
            'single character tokens' => [
                '(){},.-+;*',
                [
                    new Token(TokenType::T_LEFT_PAREN, '(', null, 0),
                    new Token(TokenType::T_RIGHT_PAREN, ')', null, 1),
                    new Token(TokenType::T_LEFT_BRACE, '{', null, 2),
                    new Token(TokenType::T_RIGHT_BRACE, '}', null, 3),
                    new Token(TokenType::T_COMMA, ',', null, 4),
                    new Token(TokenType::T_DOT, '.', null, 5),
                    new Token(TokenType::T_MINUS, '-', null, 6),
                    new Token(TokenType::T_PLUS, '+', null, 7),
                    new Token(TokenType::T_SEMICOLON, ';', null, 8),
                    new Token(TokenType::T_STAR, '*', null, 9),
                    new Token(TokenType::T_EOF, '', null, 10),
                ]
            ],
            'single or double character tokens' => [
                '!= ! == = <= < >= >',
                [
                    new Token(TokenType::T_BANG_EQUAL, '!=', null, 0),
                    new Token(TokenType::T_BANG, '!', null, 3),
                    new Token(TokenType::T_EQUAL_EQUAL, '==', null, 5),
                    new Token(TokenType::T_EQUAL, '=', null, 8),
                    new Token(TokenType::T_LESS_EQUAL, '<=', null, 10),
                    new Token(TokenType::T_LESS, '<', null, 13),
                    new Token(TokenType::T_GREATER_EQUAL, '>=', null, 15),
                    new Token(TokenType::T_GREATER, '>', null, 18),
                    new Token(TokenType::T_EOF, '', null, 19),
                ]
            ],
            'slash or comment' => [
                '/ // some comment',
                [
                    new Token(TokenType::T_SLASH, '/', null, 0),
                    new Token(TokenType::T_EOF, '', null, 17),
                ]
            ],
            'string' => [
                '"blablabla"',
                [
                    new Token(TokenType::T_STRING, '"blablabla"', 'blablabla', 0),
                    new Token(TokenType::T_EOF, '', null, 11),
                ]
            ],
            'number' => [
                '123 123.1337',
                [
                    new Token(TokenType::T_NUMBER, '123', '123', 0),
                    new Token(TokenType::T_NUMBER, '123.1337', '123.1337', 4),
                    new Token(TokenType::T_EOF, '', null, 12),
                ]
            ],
            'identifier or keyword' => [
                'test HUH and class else false fun for if nil or print return super this true var while',
                [
                    new Token(TokenType::T_IDENTIFIER, 'test', null, 0),
                    new Token(TokenType::T_IDENTIFIER, 'HUH', null, 5),
                    new Token(TokenType::T_AND, 'and', null, 9),
                    new Token(TokenType::T_CLASS, 'class', null, 13),
                    new Token(TokenType::T_ELSE, 'else', null, 19),
                    new Token(TokenType::T_FALSE, 'false', null, 24),
                    new Token(TokenType::T_FUN, 'fun', null, 30),
                    new Token(TokenType::T_FOR, 'for', null, 34),
                    new Token(TokenType::T_IF, 'if', null, 38),
                    new Token(TokenType::T_NIL, 'nil', null, 41),
                    new Token(TokenType::T_OR, 'or', null, 45),
                    new Token(TokenType::T_PRINT, 'print', null, 48),
                    new Token(TokenType::T_RETURN, 'return', null, 54),
                    new Token(TokenType::T_SUPER, 'super', null, 61),
                    new Token(TokenType::T_THIS, 'this', null, 67),
                    new Token(TokenType::T_TRUE, 'true', null, 72),
                    new Token(TokenType::T_VAR, 'var', null, 77),
                    new Token(TokenType::T_WHILE, 'while', null, 81),
                    new Token(TokenType::T_EOF, '', null, 86),
                ]
            ],
        ];
    }

    /**
     * @dataProvider scanTokensProvider
     * @param string $code
     * @param array $expected
     */
    public function testScanTokens(string $code, array $expected)
    {
        $scanner = new Scanner(new StringReader($code));
        $this->assertEquals($expected, $scanner->scanTokens());
    }
}
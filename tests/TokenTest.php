<?php
namespace Plox;

use PHPUnit\Framework\TestCase;

class TokenTest extends TestCase
{
    public function testToString()
    {
        $this->assertEquals(
            'Token{type: AND, lexeme: and, literal: <null>}',
            (string) new Token(TokenType::T_AND, 'and', null, 10)
        );

        $this->assertEquals(
            'Token{type: STRING, lexeme: "something", literal: something}',
            (string) new Token(TokenType::T_STRING, '"something"', 'something', 10)
        );
    }
}
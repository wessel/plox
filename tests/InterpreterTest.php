<?php
namespace Plox;

use PHPUnit\Framework\TestCase;
use Plox\AST\Expression\Assign;
use Plox\AST\Expression\Binary;
use Plox\AST\Expression\Call;
use Plox\AST\Expression\Expression;
use Plox\AST\Expression\Grouping;
use Plox\AST\Expression\Literal;
use Plox\AST\Expression\Logical;
use Plox\AST\Expression\Unary;
use Plox\AST\Expression\Variable;
use Plox\AST\Statement\BlockStmt;
use Plox\AST\Statement\ExpressionStmt;
use Plox\AST\Statement\IfStmt;
use Plox\AST\Statement\PrintStmt;
use Plox\AST\Statement\Statement;
use Plox\AST\Statement\VariableStmt;
use Plox\AST\Statement\WhileStmt;

class InterpreterTest extends TestCase
{
    /**
     * @return array
     */
    public function evaluateProvider(): array
    {
        return [
            'false' => [
                new Literal(false),
                false
            ],
            'true' => [
                new Literal(true),
                true
            ],
            'nil' => [
                new Literal(null),
                null
            ],
            'string' => [
                new Literal('test'),
                'test'
            ],
            'number' => [
                new Literal(1337.0),
                1337.0
            ],
            'unary minus' => [
                new Unary(new Token(TokenType::T_MINUS, '-', null, 0), new Literal(1337.0)),
                -1337.0
            ],
            'unary bang' => [
                new Unary(new Token(TokenType::T_BANG, '-', null, 0), new Literal(true)),
                false
            ],
            'grouping' => [
                new Grouping(new Literal(false)),
                false
            ],
            'binary plus numbers' => [
                new Binary(new Literal(10.0), new Literal(15.0), new Token(TokenType::T_PLUS, '+', null, 0)),
                25.0
            ],
            'binary plus strings' => [
                new Binary(new Literal('abc'), new Literal('def'), new Token(TokenType::T_PLUS, '+', null, 0)),
                'abcdef'
            ],
            'binary minus' => [
                new Binary(new Literal(15.0), new Literal(10.0), new Token(TokenType::T_MINUS, '-', null, 0)),
                5.0
            ],
            'binary slash' => [
                new Binary(new Literal(10.0), new Literal(2.0), new Token(TokenType::T_SLASH, '/', null, 0)),
                5.0
            ],
            'binary star' => [
                new Binary(new Literal(15.0), new Literal(10.0), new Token(TokenType::T_STAR, '*', null, 0)),
                150.0
            ],
            'binary greater' => [
                new Binary(new Literal(15.0), new Literal(10.0), new Token(TokenType::T_GREATER, '>', null, 0)),
                true
            ],
            'binary greater equal' => [
                new Binary(new Literal(15.0), new Literal(10.0), new Token(TokenType::T_GREATER_EQUAL, '>=', null, 0)),
                true
            ],
            'binary less' => [
                new Binary(new Literal(15.0), new Literal(10.0), new Token(TokenType::T_LESS, '<', null, 0)),
                false
            ],
            'binary less equal' => [
                new Binary(new Literal(15.0), new Literal(10.0), new Token(TokenType::T_LESS_EQUAL, '<=', null, 0)),
                false
            ],
            'logical or false false' => [
                new Logical(new Literal(false), new Token(TokenType::T_OR, 'or', null, 0), new Literal(false)),
                false
            ],
            'logical or false true' => [
                new Logical(new Literal(false), new Token(TokenType::T_OR, 'or', null, 0), new Literal(true)),
                true
            ],
            'logical or true true' => [
                new Logical(new Literal(true), new Token(TokenType::T_OR, 'or', null, 0), new Literal(true)),
                true
            ],
            'logical or true false' => [
                new Logical(new Literal(true), new Token(TokenType::T_OR, 'or', null, 0), new Literal(false)),
                true
            ],
            'logical and false false' => [
                new Logical(new Literal(false), new Token(TokenType::T_AND, 'and', null, 0), new Literal(false)),
                false
            ],
            'logical and false true' => [
                new Logical(new Literal(false), new Token(TokenType::T_AND, 'and', null, 0), new Literal(true)),
                false
            ],
            'logical and true true' => [
                new Logical(new Literal(true), new Token(TokenType::T_AND, 'and', null, 0), new Literal(true)),
                true
            ],
            'logical and true false' => [
                new Logical(new Literal(true), new Token(TokenType::T_AND, 'and', null, 0), new Literal(false)),
                false
            ],
        ];
    }

    /**
     * @dataProvider evaluateProvider
     * @param Expression $expression
     * @param $expected
     */
    public function testEvaluate(Expression $expression, $expected)
    {
        $interpreter = new Interpreter(new Environment());

        $this->assertEquals($expected, $interpreter->evaluate($expression));
    }

    /**
     * @return array
     */
    public function executeProvider(): array
    {
        return [
            'print statement' => [
                [new PrintStmt(new Literal('Hello world!'))],
                "Hello world!\n"
            ],
            'expression statement' => [
                [new ExpressionStmt(new Literal('Hello world!'))],
                ''
            ],
            'uninitialized variable' => [
                [
                    new VariableStmt(new Token(TokenType::T_IDENTIFIER, 'something', null, 0), null),
                    new PrintStmt(new Variable(new Token(TokenType::T_IDENTIFIER, 'something', null, 0))),
                ],
                "nil\n"
            ],
            'variable' => [
                [
                    new VariableStmt(new Token(TokenType::T_IDENTIFIER, 'something', null, 0), new Literal(true)),
                    new PrintStmt(new Variable(new Token(TokenType::T_IDENTIFIER, 'something', null, 0))),
                ],
                "true\n"
            ],
            'variable assignment' => [
                [
                    new VariableStmt(new Token(TokenType::T_IDENTIFIER, 'something', null, 0), new Literal(true)),
                    new ExpressionStmt(
                        new Assign(new Token(TokenType::T_IDENTIFIER, 'something', null, 0), new Literal(false))
                    ),
                    new PrintStmt(new Variable(new Token(TokenType::T_IDENTIFIER, 'something', null, 0))),
                ],
                "false\n"
            ],
            'block statement' => [
                [
                    new VariableStmt(new Token(TokenType::T_IDENTIFIER, 'outer', null, 0), new Literal('outer')),
                    new BlockStmt([
                        new PrintStmt(new Variable(new Token(TokenType::T_IDENTIFIER, 'outer', null, 0))),
                        new ExpressionStmt(new Assign(new Token(TokenType::T_IDENTIFIER, 'outer', null, 0), new Literal('inner')))
                    ]),
                    new PrintStmt(new Variable(new Token(TokenType::T_IDENTIFIER, 'outer', null, 0))),
                ],
                "outer\ninner\n"
            ],
            'if statement w/o else branch' => [
                [
                    new IfStmt(
                        new Literal(true),
                        new PrintStmt(new Literal('yes'))
                    )
                ],
                "yes\n"
            ],
            'if statement with else branch, executing the then branch' => [
                [
                    new IfStmt(
                        new Literal(true),
                        new PrintStmt(new Literal('yes')),
                        new PrintStmt(new Literal('nope')),
                    )
                ],
                "yes\n"
            ],
            'if statement with else branch, executing the else branch' => [
                [
                    new IfStmt(
                        new Literal(false),
                        new PrintStmt(new Literal('yes')),
                        new PrintStmt(new Literal('nope')),
                    )
                ],
                "nope\n"
            ],
            'while loop' => [
                [
                    new VariableStmt(new Token(TokenType::T_IDENTIFIER, 'i', null, 0), new Literal(0.0)),
                    new WhileStmt(
                        new Binary(new Variable(new Token(TokenType::T_IDENTIFIER, 'i', null, 0)), new Literal(3.0), new Token(TokenType::T_LESS, '<', null, 0)),
                        new BlockStmt([
                            new PrintStmt(new Variable(new Token(TokenType::T_IDENTIFIER, 'i', null, 0))),
                            new ExpressionStmt(
                                new Assign(
                                    new Token(TokenType::T_IDENTIFIER, 'i', null, 0),
                                    new Binary(
                                        new Variable(new Token(TokenType::T_IDENTIFIER, 'i', null, 0)),
                                        new Literal(1.0),
                                        new Token(TokenType::T_PLUS, '+', null, 0)
                                    )
                                )
                            ),
                        ])
                    )
                ],
                "0\n1\n2\n"
            ],
        ];
    }

    /**
     * @dataProvider executeProvider
     * @param Statement[] $statements
     * @param string $expected
     */
    public function testExecute(array $statements, string $expected)
    {
        $interpreter = new Interpreter(new Environment());

        ob_start();
        $interpreter->interpret($statements);
        $buffer = ob_get_clean();

        $this->assertEquals($expected, $buffer);
    }

    public function testFunctions()
    {
        $testFunction = $this->createMock(LoxCallable::class);

        $testFunction->method('arity')->willReturn(0);
        $testFunction->method('call')->willReturnCallback(function (Interpreter $interpreter, array $arguments) {
            return "success";
        });

        $globals = new Environment();
        $globals->define('testFunction', $testFunction);

        $interpreter = new Interpreter($globals);

        ob_start();
        $interpreter->interpret([
            new PrintStmt(
                new Call(
                    new Variable(new Token(TokenType::T_IDENTIFIER, 'testFunction', null, 0)),
                    []
                )
            )
        ]);
        $buffer = ob_get_clean();

        $this->assertEquals("success\n", $buffer);

    }
}
<?php
namespace Plox;

use PHPUnit\Framework\TestCase;
use Plox\AST\Expression\Assign;
use Plox\AST\Expression\Binary;
use Plox\AST\Expression\Call;
use Plox\AST\Expression\Expression;
use Plox\AST\Expression\Grouping;
use Plox\AST\Expression\Literal;
use Plox\AST\Expression\Logical;
use Plox\AST\Expression\Unary;
use Plox\AST\Expression\Variable;
use Plox\AST\Statement\BlockStmt;
use Plox\AST\Statement\ExpressionStmt;
use Plox\AST\Statement\FunctionStmt;
use Plox\AST\Statement\IfStmt;
use Plox\AST\Statement\PrintStmt;
use Plox\AST\Statement\ReturnStmt;
use Plox\AST\Statement\Statement;
use Plox\AST\Statement\VariableStmt;
use Plox\AST\Statement\WhileStmt;
use Plox\Reader\TokenReader;

class ParserTest extends TestCase
{
    /**
     * @return array
     */
    public function expressionsProvider(): array
    {
        return [
            'primary false' => [
                [new Token(TokenType::T_FALSE, 'false', null, 0)],
                new Literal(false),
            ],
            'primary true' => [
                [new Token(TokenType::T_TRUE, 'true', null, 0)],
                new Literal(true),
            ],
            'primary nil' => [
                [new Token(TokenType::T_NIL, 'nil', null, 0)],
                new Literal(null),
            ],
            'primary string' => [
                [new Token(TokenType::T_STRING, '"HUH"', 'HUH', 0)],
                new Literal('HUH'),
            ],
            'primary number' => [
                [new Token(TokenType::T_NUMBER, '1337', '1337', 0)],
                new Literal(1337),
            ],
            'primary grouping' => [
                [
                    new Token(TokenType::T_LEFT_PAREN, '(', null, 0),
                    new Token(TokenType::T_TRUE, 'true', null, 0),
                    new Token(TokenType::T_RIGHT_PAREN, ')', null, 0),
                ],
                new Grouping(new Literal(true)),
            ],
            'primary identifier' => [
                [
                    new Token(TokenType::T_IDENTIFIER, 'huh', null, 0),
                ],
                new Variable(new Token(TokenType::T_IDENTIFIER, 'huh', null, 0)),
            ],
            'unary bang' => [
                [
                    new Token(TokenType::T_BANG, '!', null, 0),
                    new Token(TokenType::T_TRUE, 'true', null, 0),
                ],
                new Unary(new Token(TokenType::T_BANG, '!', null, 0), new Literal(true)),
            ],
            'unary minus' => [
                [
                    new Token(TokenType::T_MINUS, '-', null, 0),
                    new Token(TokenType::T_TRUE, 'true', null, 0),
                ],
                new Unary(new Token(TokenType::T_MINUS, '-', null, 0), new Literal(true)),
            ],
            'multiplication slash' => [
                [
                    new Token(TokenType::T_NUMBER, '10', '10', 0),
                    new Token(TokenType::T_SLASH, '/', null, 0),
                    new Token(TokenType::T_NUMBER, '15', '15', 0),
                ],
                new Binary(new Literal(10), new Literal(15), new Token(TokenType::T_SLASH, '/', null, 0)),
            ],
            'multiplication star' => [
                [
                    new Token(TokenType::T_NUMBER, '10', '10', 0),
                    new Token(TokenType::T_STAR, '*', null, 0),
                    new Token(TokenType::T_NUMBER, '15', '15', 0),
                ],
                new Binary(new Literal(10), new Literal(15), new Token(TokenType::T_STAR, '*', null, 0)),
            ],
            'addition plus' => [
                [
                    new Token(TokenType::T_NUMBER, '10', '10', 0),
                    new Token(TokenType::T_PLUS, '+', null, 0),
                    new Token(TokenType::T_NUMBER, '15', '15', 0),
                ],
                new Binary(new Literal(10), new Literal(15), new Token(TokenType::T_PLUS, '+', null, 0)),
            ],
            'addition minus' => [
                [
                    new Token(TokenType::T_NUMBER, '10', '10', 0),
                    new Token(TokenType::T_MINUS, '-', null, 0),
                    new Token(TokenType::T_NUMBER, '15', '15', 0),
                ],
                new Binary(new Literal(10), new Literal(15), new Token(TokenType::T_MINUS, '-', null, 0)),
            ],
            'comparison greater' => [
                [
                    new Token(TokenType::T_NUMBER, '10', '10', 0),
                    new Token(TokenType::T_GREATER, '>', null, 0),
                    new Token(TokenType::T_NUMBER, '15', '15', 0),
                ],
                new Binary(new Literal(10), new Literal(15), new Token(TokenType::T_GREATER, '>', null, 0)),
            ],
            'comparison greater equal' => [
                [
                    new Token(TokenType::T_NUMBER, '10', '10', 0),
                    new Token(TokenType::T_GREATER_EQUAL, '>=', null, 0),
                    new Token(TokenType::T_NUMBER, '15', '15', 0),
                ],
                new Binary(new Literal(10), new Literal(15), new Token(TokenType::T_GREATER_EQUAL, '>=', null, 0)),
            ],
            'comparison less' => [
                [
                    new Token(TokenType::T_NUMBER, '10', '10', 0),
                    new Token(TokenType::T_LESS, '<', null, 0),
                    new Token(TokenType::T_NUMBER, '15', '15', 0),
                ],
                new Binary(new Literal(10), new Literal(15), new Token(TokenType::T_LESS, '<', null, 0)),
            ],
            'comparison less equal' => [
                [
                    new Token(TokenType::T_NUMBER, '10', '10', 0),
                    new Token(TokenType::T_LESS_EQUAL, '<=', null, 0),
                    new Token(TokenType::T_NUMBER, '15', '15', 0),
                ],
                new Binary(new Literal(10), new Literal(15), new Token(TokenType::T_LESS_EQUAL, '<=', null, 0)),
            ],
            'equality bang equal' => [
                [
                    new Token(TokenType::T_NUMBER, '10', '10', 0),
                    new Token(TokenType::T_BANG_EQUAL, '!=', null, 0),
                    new Token(TokenType::T_NUMBER, '15', '15', 0),
                ],
                new Binary(new Literal(10), new Literal(15), new Token(TokenType::T_BANG_EQUAL, '!=', null, 0)),
            ],
            'equality equal equal' => [
                [
                    new Token(TokenType::T_NUMBER, '10', '10', 0),
                    new Token(TokenType::T_EQUAL_EQUAL, '==', null, 0),
                    new Token(TokenType::T_NUMBER, '15', '15', 0),
                ],
                new Binary(new Literal(10), new Literal(15), new Token(TokenType::T_EQUAL_EQUAL, '==', null, 0)),
            ],
            'assignment' => [
                [
                    new Token(TokenType::T_IDENTIFIER, 'something', null, 0),
                    new Token(TokenType::T_EQUAL, '=', null, 0),
                    new Token(TokenType::T_TRUE, 'true', null, 0),
                ],
                new Assign(new Token(TokenType::T_IDENTIFIER, 'something', null, 0), new Literal(true)),
            ],
            'logical or' => [
                [
                    new Token(TokenType::T_TRUE, 'true', null, 0),
                    new Token(TokenType::T_OR, 'or', null, 0),
                    new Token(TokenType::T_TRUE, 'true', null, 0),
                ],
                new Logical(new Literal(true), new Token(TokenType::T_OR, 'or', null, 0), new Literal(true))
            ],
            'logical and' => [
                [
                    new Token(TokenType::T_TRUE, 'true', null, 0),
                    new Token(TokenType::T_AND, 'and', null, 0),
                    new Token(TokenType::T_TRUE, 'true', null, 0),
                ],
                new Logical(new Literal(true), new Token(TokenType::T_AND, 'and', null, 0), new Literal(true))
            ],
            'function call w/o arguments' => [
                [
                    new Token(TokenType::T_IDENTIFIER, 'function', null, 0),
                    new Token(TokenType::T_LEFT_PAREN, '(', null, 0),
                    new Token(TokenType::T_RIGHT_PAREN, '(', null, 0),
                ],
                new Call(new Variable(new Token(TokenType::T_IDENTIFIER, 'function', null, 0)), [])
            ],
            'function call with arguments' => [
                [
                    new Token(TokenType::T_IDENTIFIER, 'function', null, 0),
                    new Token(TokenType::T_LEFT_PAREN, '(', null, 0),
                    new Token(TokenType::T_TRUE, 'true', null, 0),
                    new Token(TokenType::T_COMMA, ',', null, 0),
                    new Token(TokenType::T_FALSE, 'false', null, 0),
                    new Token(TokenType::T_RIGHT_PAREN, '(', null, 0),
                ],
                new Call(
                    new Variable(new Token(TokenType::T_IDENTIFIER, 'function', null, 0)),
                    [new Literal(true), new Literal(false)]
                )
            ],
        ];
    }

    /**
     * @dataProvider  expressionsProvider
     * @param Token[] $tokens
     * @param Expression $expected
     */
    public function testExpressions(array $tokens, Expression $expected)
    {
        $parser = new Parser(new TokenReader($tokens));

        $this->assertEquals($expected, $parser->expression());
    }

    /**
     * @return array
     */
    public function statementsProvider(): array
    {
        return [
            'simple expression statement' => [
                [
                    new Token(TokenType::T_FALSE, 'false', null, 0),
                    new Token(TokenType::T_SEMICOLON, ';', null, 0),
                ],
                [
                    new ExpressionStmt(new Literal(false)),
                ]
            ],
            'simple print statement' => [
                [
                    new Token(TokenType::T_PRINT, 'print', null, 0),
                    new Token(TokenType::T_FALSE, 'false', null, 0),
                    new Token(TokenType::T_SEMICOLON, ';', null, 0),
                ],
                [
                    new PrintStmt(new Literal(false)),
                ]
            ],
            'variable declaration w/o initializer' => [
                [
                    new Token(TokenType::T_VAR, 'var', null, 0),
                    new Token(TokenType::T_IDENTIFIER, 'test', null, 0),
                    new Token(TokenType::T_SEMICOLON, ';', null, 0),
                ],
                [
                    new VariableStmt(new Token(TokenType::T_IDENTIFIER, 'test', null, 0), null)
                ]
            ],
            'variable declaration with initializer' => [
                [
                    new Token(TokenType::T_VAR, 'var', null, 0),
                    new Token(TokenType::T_IDENTIFIER, 'test', null, 0),
                    new Token(TokenType::T_EQUAL, '=', null, 0),
                    new Token(TokenType::T_TRUE, 'true', null, 0),
                    new Token(TokenType::T_SEMICOLON, ';', null, 0),
                ],
                [
                    new VariableStmt(
                        new Token(TokenType::T_IDENTIFIER, 'test', null, 0),
                        new Literal(true)
                    )
                ]
            ],
            'block' => [
                [
                    new Token(TokenType::T_LEFT_BRACE, '{', null, 0),
                    new Token(TokenType::T_TRUE, 'true', null, 0),
                    new Token(TokenType::T_SEMICOLON, ';', null, 0),
                    new Token(TokenType::T_FALSE, 'false', null, 0),
                    new Token(TokenType::T_SEMICOLON, ';', null, 0),
                    new Token(TokenType::T_RIGHT_BRACE, '}', null, 0),
                ],
                [
                    new BlockStmt([
                        new ExpressionStmt(new Literal(true)),
                        new ExpressionStmt(new Literal(false)),
                    ])
                ]
            ],
            'if statement w/o else branch' => [
                [
                    new Token(TokenType::T_IF, 'if', null, 0),
                    new Token(TokenType::T_LEFT_PAREN, '(', null, 0),
                    new Token(TokenType::T_TRUE, 'true', null, 0),
                    new Token(TokenType::T_RIGHT_PAREN, ')', null, 0),
                    new Token(TokenType::T_TRUE, 'true', null, 0),
                    new Token(TokenType::T_SEMICOLON, ';', null, 0),
                ],
                [new IfStmt(new Literal(true), new ExpressionStmt(new Literal(true)))]
            ],
            'if statement with else branch' => [
                [
                    new Token(TokenType::T_IF, 'if', null, 0),
                    new Token(TokenType::T_LEFT_PAREN, '(', null, 0),
                    new Token(TokenType::T_TRUE, 'true', null, 0),
                    new Token(TokenType::T_RIGHT_PAREN, ')', null, 0),
                    new Token(TokenType::T_TRUE, 'true', null, 0),
                    new Token(TokenType::T_SEMICOLON, ';', null, 0),
                    new Token(TokenType::T_ELSE, 'else', null, 0),
                    new Token(TokenType::T_FALSE, 'false', null, 0),
                    new Token(TokenType::T_SEMICOLON, ';', null, 0),
                ],
                [new IfStmt(new Literal(true), new ExpressionStmt(new Literal(true)), new ExpressionStmt(new Literal(false)))]
            ],
            'while statement' => [
                [
                    new Token(TokenType::T_WHILE, 'while', null, 0),
                    new Token(TokenType::T_LEFT_PAREN, '(', null, 0),
                    new Token(TokenType::T_TRUE, '(', null, 0),
                    new Token(TokenType::T_RIGHT_PAREN, ')', null, 0),
                    new Token(TokenType::T_FALSE, '(', null, 0),
                    new Token(TokenType::T_SEMICOLON, ';', null, 0),
                ],
                [new WhileStmt(new Literal(true), new ExpressionStmt(new Literal(false)))]
            ],
            'function declaration w/o parameters' => [
                [
                    new Token(TokenType::T_FUN, 'fun', null, 0),
                    new Token(TokenType::T_IDENTIFIER, 'testFunction', null, 0),
                    new Token(TokenType::T_LEFT_PAREN, '(', null, 0),
                    new Token(TokenType::T_RIGHT_PAREN, ')', null, 0),
                    new Token(TokenType::T_LEFT_BRACE, '{', null, 0),
                    new Token(TokenType::T_RIGHT_BRACE, '}', null, 0),
                ],
                [new FunctionStmt(
                    new Token(TokenType::T_IDENTIFIER, 'testFunction', null, 0),
                    [],
                    []
                )]
            ],
            'function declaration with parameters' => [
                [
                    new Token(TokenType::T_FUN, 'fun', null, 0),
                    new Token(TokenType::T_IDENTIFIER, 'testFunction', null, 0),
                    new Token(TokenType::T_LEFT_PAREN, '(', null, 0),
                    new Token(TokenType::T_IDENTIFIER, 'first', null, 0),
                    new Token(TokenType::T_COMMA, ',', null, 0),
                    new Token(TokenType::T_IDENTIFIER, 'second', null, 0),
                    new Token(TokenType::T_RIGHT_PAREN, ')', null, 0),
                    new Token(TokenType::T_LEFT_BRACE, '{', null, 0),
                    new Token(TokenType::T_RIGHT_BRACE, '}', null, 0),
                ],
                [new FunctionStmt(
                    new Token(TokenType::T_IDENTIFIER, 'testFunction', null, 0),
                    [
                        new Token(TokenType::T_IDENTIFIER, 'first', null, 0),
                        new Token(TokenType::T_IDENTIFIER, 'second', null, 0),
                    ],
                    []
                )]
            ],
            'return statement' => [
                [
                    new Token(TokenType::T_RETURN, 'return', null, 0),
                    new Token(TokenType::T_TRUE, 'true', null, 0),
                    new Token(TokenType::T_SEMICOLON, ';', null, 0),
                ],
                [new ReturnStmt(new Token(TokenType::T_RETURN, 'return', null, 0), new Literal(true))]
            ]
        ];
    }

    /**
     * @dataProvider statementsProvider
     * @param Token[] $tokens
     * @param Statement[] $expected
     */
    public function testStatements(array $tokens, array $expected)
    {
        $parser = new Parser(new TokenReader($tokens));

        $this->assertEquals($expected, $parser->parse());
    }
}
<?php
namespace Plox;

use PHPUnit\Framework\TestCase;

class EnvironmentTest extends TestCase
{
    public function testDefine()
    {
        $environment = new Environment();

        $environment->define('test', 'something');
        $this->assertEquals('something', $environment->get('test'));
    }

    public function testCanDefineOnlyOnce()
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage("Variable 'test' is already defined");

        $environment = new Environment();
        $environment->define('test', 'something');
        $environment->define('test', 'something');
    }

    public function testCanDefineNull()
    {
        $environment = new Environment();
        $environment->define('something', null);
        $this->assertNull($environment->get('something'));
    }

    public function testGetThrowsExceptionIfNotDefined()
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage("Undefined variable 'test'");

        $environment = new Environment();
        $environment->get('test');
    }

    public function testAssignNotDefined()
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage("Variable 'var' is not defined");

        $environment = new Environment();
        $environment->assign('var', 'iets');
    }

    public function testAssign()
    {
        $environment = new Environment();
        $environment->define('var', 'something');

        $this->assertEquals('something', $environment->get('var'));

        $environment->assign('var', 'something else');
        $this->assertEquals('something else', $environment->get('var'));
    }

    public function testEnclosing()
    {
        $enclosing = new Environment();
        $enclosing->define('enclosing', 1337);

        $environment = new Environment($enclosing);

        $this->assertEquals(1337, $environment->get('enclosing'));

        $environment->assign('enclosing', 'outer');

        $this->assertEquals('outer', $environment->get('enclosing'));
        $this->assertEquals('outer', $enclosing->get('enclosing'));

        $environment->define('enclosing', 'inner');

        $this->assertEquals('inner', $environment->get('enclosing'));
        $this->assertEquals('outer', $enclosing->get('enclosing'));
    }
}
<?php
namespace PloxTest;

use PHPUnit\Framework\TestCase;
use Plox\Interpreter;
use Plox\Parser;
use Plox\Reader\StringReader;
use Plox\Reader\TokenReader;
use Plox\Scanner;
use Plox\STL\Factory;

class IntegrationTest extends TestCase
{
    public function integrationProvider(): array
    {
        return [
            ['print (3 + 5) * 4;', "32\n"],
            ['print 5 * (3 * (2 + 2));', "60\n"],
            ['print "abc" + "def";', "abcdef\n"],
            [
                '
                var a = 10;
                var b = 15;
                print a + b;
                ',
                "25\n",
            ],
            [
                '
                var a = 10;
                a = 1337;
                print a;
                ',
                "1337\n",
            ],
            [
                '
                var a = "global a";
                {
                  var a = "outer a";
                  {
                    var a = "inner a";
                    print a;
                  }
                  print a;
                }
                print a;
                ',
                "inner a\nouter a\nglobal a\n"
            ],
            [
                '
                print "hi" or 2; // "hi".
                print nil or "yes"; // "yes".
                ',
                "hi\nyes\n"
            ],
            [
                '
                {
                  var i = 0;
                  while (i < 5) {
                    print i;
                    i = i + 1;
                  }
                }
                ',
                "0\n1\n2\n3\n4\n"
            ],
            [
                'print loxinfo();',
                "Lox interpreter written in PHP (because why the fuck not)\n"
            ],
            [
                '
                fun sayHi(first, last) {
                  print "Hi, " + first + " " + last + "!";
                }
                
                sayHi("Dear", "Reader");
                ',
                "Hi, Dear Reader!\n"
            ],
            [
                '
                fun fibonacci(n) {
                  if (n <= 1) return n;
                  return fibonacci(n - 2) + fibonacci(n - 1);
                }
                
                var i = 0;
                var buffer = "";
                
                while (i < 20) {
                    var separator = "";
                    
                    if (i > 0) {
                        separator = ", "; 
                    }
                    
                    buffer = buffer + separator + fibonacci(i);
                    i = i + 1;
                }
                
                print buffer;
                ',
                "0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181\n"
            ],
            [
                '
                fun makeCounter() {
                  var i = 0;
                  fun count() {
                    i = i + 1;
                    print i;
                  }
                
                  return count;
                }
                
                var counter = makeCounter();
                counter(); // "1".
                counter(); // "2".
                ',
                "1\n2\n"
            ]
        ];
    }

    /**
     * @dataProvider integrationProvider
     * @param string $input
     * @param $expected
     */
    public function test(string $input, $expected)
    {
        $scanner = new Scanner(new StringReader($input));
        $tokens = $scanner->scanTokens();
        $parser = new Parser(new TokenReader($tokens));
        $statements = $parser->parse();

        $globals = Factory::create();
        $interpreter = new Interpreter($globals);

        ob_start();
        $interpreter->interpret($statements);
        $buffer = ob_get_clean();

        $this->assertEquals($expected, $buffer);
    }
}
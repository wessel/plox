<?php
namespace PloxTest\Reader;

use PHPUnit\Framework\TestCase;
use Plox\Reader\StringReader;

class StringReaderTest extends TestCase
{
    public function testAdvance()
    {
        $reader = new StringReader('abc');

        $this->assertEquals(0, $reader->getCursor());

        $this->assertEquals('a', $reader->advance());
        $this->assertEquals(1, $reader->getCursor());

        $this->assertEquals('b', $reader->advance());
        $this->assertEquals(2, $reader->getCursor());

        $this->assertEquals('c', $reader->advance());
        $this->assertEquals(3, $reader->getCursor());

        $this->assertEquals(null, $reader->advance());
        $this->assertEquals(3, $reader->getCursor());

        $this->assertEquals(null, $reader->advance());
        $this->assertEquals(3, $reader->getCursor());
    }

    public function testPeek()
    {
        $reader = new StringReader('ab');

        $this->assertEquals(0, $reader->getCursor());
        $this->assertEquals('a', $reader->peek());
        $this->assertEquals(0, $reader->getCursor());

        $reader->advance();

        $this->assertEquals(1, $reader->getCursor());
        $this->assertEquals('b', $reader->peek());
        $this->assertEquals(1, $reader->getCursor());

        $reader->advance();

        $this->assertEquals(2, $reader->getCursor());
        $this->assertEquals(null, $reader->peek());
        $this->assertEquals(2, $reader->getCursor());
    }

    public function testIsAtEnd()
    {
        $this->assertTrue((new StringReader(''))->isAtEnd());

        $reader = new StringReader("ab");

        $this->assertFalse($reader->isAtEnd());
        $reader->advance();
        $this->assertFalse($reader->isAtEnd());
        $reader->advance();
        $this->assertTrue($reader->isAtEnd());
    }

    public function testMatch()
    {
        $reader = new StringReader("abcd");

        $this->assertEquals(0, $reader->getCursor());
        $this->assertFalse($reader->match('b'));
        $this->assertEquals(0, $reader->getCursor());
        $this->assertTrue($reader->match('a'));
        $this->assertEquals(1, $reader->getCursor());

    }

    public function testSelection()
    {
        $reader = new StringReader('abcdefg');

        $reader->startSelection();
        $reader->advance();

        $this->assertEquals('a', $reader->endSelection());

        $reader->advance();
        $reader->startSelection();
        $reader->advance();
        $reader->advance();

        $this->assertEquals('cd', $reader->endSelection());

        $reader->startSelection();
        $reader->advance();
        $reader->advance();
        $reader->advance();

        $this->assertEquals('efg', $reader->endSelection());
    }
}
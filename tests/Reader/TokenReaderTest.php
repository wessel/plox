<?php
namespace PloxTest\Reader;

use PHPUnit\Framework\TestCase;
use Plox\Reader\TokenReader;
use Plox\Token;
use Plox\TokenType;

class TokenReaderTest extends TestCase
{
    public function testAdvance()
    {
        $tokens = [
            new Token(TokenType::T_DOT, '.', null, 0),
            new Token(TokenType::T_MINUS, '-', null, 0),
        ];

        $reader = new TokenReader($tokens);

        $this->assertEquals(0, $reader->getCursor());
        $this->assertEquals($tokens[0], $reader->advance());
        $this->assertEquals(1, $reader->getCursor());
        $this->assertEquals($tokens[1], $reader->advance());
        $this->assertEquals(2, $reader->getCursor());
        $this->assertEquals(new Token(TokenType::T_EOF, '', null, -1), $reader->advance());
        $this->assertEquals(2, $reader->getCursor());
    }

    public function testPeek()
    {
        $tokens = [
            new Token(TokenType::T_DOT, '.', null, 0),
            new Token(TokenType::T_MINUS, '-', null, 0),
        ];

        $reader = new TokenReader($tokens);

        $this->assertEquals(0, $reader->getCursor());
        $this->assertEquals($tokens[0], $reader->peek());
        $this->assertEquals(0, $reader->getCursor());

        $reader->advance();

        $this->assertEquals(1, $reader->getCursor());
        $this->assertEquals($tokens[1], $reader->peek());
        $this->assertEquals(1, $reader->getCursor());

        $reader->advance();

        $this->assertEquals(2, $reader->getCursor());
        $this->assertEquals(new Token(TokenType::T_EOF, '', null, -1), $reader->peek());
        $this->assertEquals(2, $reader->getCursor());
    }

    public function testCheck()
    {
        $tokens = [
            new Token(TokenType::T_DOT, '.', null, 0),
            new Token(TokenType::T_MINUS, '-', null, 0),
        ];

        $reader = new TokenReader($tokens);

        $this->assertFalse($reader->check(TokenType::T_MINUS));
        $this->assertTrue($reader->check(TokenType::T_DOT));

        $reader->advance();

        $this->assertTrue($reader->check(TokenType::T_MINUS));
        $this->assertFalse($reader->check(TokenType::T_DOT));

        $reader->advance();

        $this->assertTrue($reader->check(TokenType::T_EOF));
    }

    public function testMatch()
    {
        $tokens = [
            new Token(TokenType::T_DOT, '.', null, 0),
            new Token(TokenType::T_MINUS, '-', null, 0),
        ];

        $reader = new TokenReader($tokens);

        $this->assertFalse($reader->match(TokenType::T_MINUS));
        $this->assertTrue($reader->match(TokenType::T_DOT));

        $this->assertFalse($reader->match(TokenType::T_DOT));
        $this->assertTrue($reader->match(TokenType::T_MINUS));

        $this->assertTrue($reader->match(TokenType::T_EOF));
    }

    public function testIsAtEnd()
    {
        $this->assertTrue((new TokenReader([]))->isAtEnd());
        $this->assertTrue((new TokenReader([new Token(TokenType::T_EOF, '', null, 0)]))->isAtEnd());
        $this->assertFalse((new TokenReader([new Token(TokenType::T_DOT, '.', null, 0)]))->isAtEnd());

    }
}
#!/usr/bin/php
<?php
use Plox\Interpreter;
use Plox\Parser;
use Plox\Reader\StringReader;
use Plox\Reader\TokenReader;
use Plox\Scanner;
use Plox\STL\Factory;

require_once dirname(__DIR__) . '/vendor/autoload.php';

if ($argc !== 2) {
    echo "Usage: lox.php /path/to/source", PHP_EOL;
    exit(-1);
}

$source = file_get_contents($argv[1]);

if ($source === false) {
    echo "Could not open file: " . $argv[1], PHP_EOL;
    exit(-1);
}

$scanner = new Scanner(new StringReader($source));
$tokens = $scanner->scanTokens();
$parser = new Parser(new TokenReader($tokens));
$statements = $parser->parse();

$globals = Factory::create();
$interpreter = new Interpreter($globals);
$interpreter->interpret($statements);
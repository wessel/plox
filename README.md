## Grammar

```
program     → declaration* EOF ;

declaration → funDecl
            | varDecl
            | statement ;

funDecl  → "fun" function ;
function → IDENTIFIER "(" parameters? ")" block ;
parameters → IDENTIFIER ( "," IDENTIFIER )* ;

varDecl → "var" IDENTIFIER ( "=" expression )? ";" ;

statement → exprStmt
          | ifStmt
          | returnStmt
          | whileStmt
          | printStmt
          | block ;

ifStmt    → "if" "(" expression ")" statement ( "else" statement )? ;
returnStmt → "return" expression? ";" ;
whileStmt → "while" "(" expression ")" statement ;
exprStmt  → expression ";" ;
printStmt → "print" expression ";" ;
block     → "{" declaration* "}" ;

expression → assignment ;
assignment → identifier "=" assignment
           | logic_or ;
logic_or   → logic_and ( "or" logic_and )* ;
logic_and  → equality ( "and" equality )* ;
equality       → comparison ( ( "!=" | "==" ) comparison )* ;
comparison     → addition ( ( ">" | ">=" | "<" | "<=" ) addition )* ;
addition       → multiplication ( ( "-" | "+" ) multiplication )* ;
multiplication → unary ( ( "/" | "*" ) unary )* ;
unary → ( "!" | "-" ) unary | call ;
call  → primary ( "(" arguments? ")" )* ;
arguments → expression ( "," expression )* ;
primary        → NUMBER | STRING | "false" | "true" | "nil"
               | "(" expression ")" 
               | IDENTIFIER;
```